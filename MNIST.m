%simply hit 'run' to run the program, it may take long time
clc
clear
%load data
load('MNIST_Xtestp.mat')
load('MNIST_Xtrain.mat')
load('MNIST_ytrain.mat')


%generate a larger sample to train
i = 1;
Xnew = [];
Ynew = [];
for i = 1:200
    com = [Xtrain ytrain];
    randp = randperm(60000);
    comout = com(randp,:);
    X = comout(:,1:784);
    Y = comout(:,785);
    Xnew = [Xnew;X];
    Ynew = [Ynew;Y];
    i = i+1;
end


%use 6nn (6nn is the best I can find)
Mdl = fitcknn(Xnew,Ynew,'NumNeighbors',6);
label = predict(Mdl,Xtest);



%attach another column of image ID
predwlab = [(1:10000)' label];
%write in CSV file
csvwrite('20502755',[]);
%write header
cHeader = {'ImageID' 'Digit'};
textHeader = strjoin(cHeader, ',');
fid = fopen('20502755.csv','w');
fprintf(fid,'%s\n',textHeader);
fclose(fid);
dlmwrite('20502755.csv',predwlab,'-append');